﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GGJ
{

    public class PointsGroup : MonoBehaviour
    {
        [SerializeField] private GameObject pointPrefab;
        [SerializeField] private Vector2 respawnTimeRange = new Vector2(0, 100);

        List<GameObject> pointsInGroup = new List<GameObject>();
        bool respawning = false;

        CoroutineHandle respawnHandle;

        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                var pointGO = Instantiate(pointPrefab, transform.GetChild(i).position, Quaternion.identity, transform.GetChild(i));
                pointGO.GetComponentInChildren<PointValue>().owningGroup = this;
                pointGO.SetActive(false);
                pointsInGroup.Add(pointGO);
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.F12))
            {
                if (respawnHandle.IsRunning)
                    Timing.KillCoroutines(respawnHandle);
                respawning = false;
                SpawnInitalPoint();
            }
        }

        public void SpawnInitalPoint()
        {
            respawning = false;
            int target = Random.Range(0, pointsInGroup.Count);
            pointsInGroup[target].SetActive(true);
        }

        public void HideAllPoints()
        {
            foreach(var go in pointsInGroup)
            {
                go.SetActive(false);
            }
            if(respawnHandle.IsRunning)
                Timing.KillCoroutines(respawnHandle);
            respawning = false;
        }

        public void RespawnNewPoint(GameObject previousPoint)
        {
            if (!respawning)
            {
                previousPoint.SetActive(false);
                respawnHandle = Timing.RunCoroutine(Respawn());
            }
        }

        IEnumerator<float> Respawn()
        {
            respawning = true;
            float waitTime = Random.Range(respawnTimeRange.x, respawnTimeRange.y) / GameManager.Instance.PlayerCount;
            yield return Timing.WaitForSeconds(waitTime);
            int target = Random.Range(0, pointsInGroup.Count);
            pointsInGroup[target].SetActive(true);
            respawning = false;
        }


        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            float size = 0.5f;
            var color = Color.green;
            var colorAlphaed = Color.green;
            
            if (pointPrefab)
            {
                size = pointPrefab.GetComponentInChildren<MeshRenderer>().bounds.extents.magnitude;
                color = colorAlphaed = pointPrefab.GetComponent<PointValue>().debugColor;
            }

            colorAlphaed.a = 0.65f;

            for (int i = 0; i < transform.childCount; ++i)
            {
                Gizmos.color = colorAlphaed;
                Gizmos.DrawSphere(transform.GetChild(i).position, size);
                Gizmos.color = color;
                Gizmos.DrawWireSphere(transform.GetChild(i).position, size);
            }

#endif
        }

    }

}