﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GGJ
{

	public class LoadingManager : MonoBehaviour
	{
		private static LoadingManager instance;
		public static LoadingManager Instance => instance;

		[SerializeField] private float fadeDuration = 0.5f;
		
		[SerializeField] private CanvasGroup canvasGrounp;
		[SerializeField] private TextMeshProUGUI loadingPercentageText;
		[SerializeField] private TextMeshProUGUI targetSceneText;

		private CoroutineHandle loadingHandle;
		
		
		void Awake()
		{
			if(instance)
				Destroy(gameObject);
			else
			{
				DontDestroyOnLoad(gameObject);
				instance = this;
			}
		}

		public void LoadScene(string targetScene)
		{
			if (loadingHandle.IsRunning)
				return;
			loadingHandle =Timing.RunCoroutine(LoadSceneInternal(targetScene));
		}

		IEnumerator<float> LoadSceneInternal(string targetScene)
		{
			float internalTimer = 0f;
			canvasGrounp.alpha = 0f;
			loadingPercentageText.text = $"Loading  0%";
			targetSceneText.text = targetScene;
			
			while (internalTimer < fadeDuration)
			{
				internalTimer += Time.deltaTime;
				canvasGrounp.alpha = internalTimer / fadeDuration;
				yield return Timing.WaitForOneFrame;
			}

			var ao = SceneManager.LoadSceneAsync(targetScene, LoadSceneMode.Single);
			while (!ao.isDone)
			{
				loadingPercentageText.text = $"Loading  {Mathf.RoundToInt(ao.progress * 100)}%";
				yield return Timing.WaitForOneFrame;
			}

			internalTimer = 0f;
			
			while (internalTimer < fadeDuration)
			{
				internalTimer += Time.deltaTime;
				canvasGrounp.alpha = 1 - (internalTimer / fadeDuration);
				yield return Timing.WaitForOneFrame;
			}

			//GameManager.Instance.StartGameFromMenuPressed();
		}
	}

}