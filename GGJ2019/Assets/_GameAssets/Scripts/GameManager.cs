﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using System.Linq;
using System;
using Rewired;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace GGJ
{
    enum GameState
    {
        Startup,
        InGame,
        GameOver
    }
    
    public class GameManager : MonoBehaviour
    {
        static GameManager instance;
        public static GameManager Instance => instance;

        [Header("Config")]
        [Space]
        [Range(1, 10)][SerializeField] private int maxRounds = 3;
        int currentRound = 0;
        [Range(0f, 5f)][SerializeField] private int secondsBeforeRoundStarts = 3;

        

        [Space]
        [SerializeField][Range(20f, 60f)] private float initialRoundTime = 30f;
        [SerializeField][Range(30f, 120f)] private float minRoundTime = 30f;
        [SerializeField][Range(30f, 120f)] private float maxRoundTime = 60f;
        [SerializeField][Range(5f, 20f)] private float getHomeTime = 10f;
        float roundDuration = 0f;
        [SerializeField] private int outOfZoneNightPenaltyPerTick = 5;
        [SerializeField] private float outOfZoneNightTickDuration = 1.5f;
        [SerializeField] private int[] lostPlayersPointReductionPerWave = new int[]{
            5, 15, 30
        };
        
        [Header("Prefabs")]
        [SerializeField] private GameObject playerPrefab;
        
        [Header("Safe Indicators")]
        [SerializeField] private GameObject safeSpaceCounter;
       // [SerializeField] private Transform safeSpaceTransform;
        [SerializeField] private float seperationDistance;
        [SerializeField] private float safeIndicatorSize;
        [SerializeField] private Color safeSpaceUsedColor;
        [SerializeField] private Color safeSpaceUnUsedColor;
        

        [Header("UI")]
        [SerializeField] private GameObject scoreTextPrefab;
        [SerializeField] private Transform scoreTextContainer;

        [Header("Day / Night Cycle")]
        [SerializeField] private Lightening lightening;
        [SerializeField] private Light sun;
        [SerializeField] private Color dayColor = new Color(0.976f, 0.964f, 0.862f);
        [SerializeField] private Color nightColor = new Color(0.168f, 0.317f, 0.450f);
        [SerializeField] private float dayIntensity = 1.5f;
        [SerializeField] private float nightIntensity = 0.5f;
        [SerializeField] private float dayNightTransitionDuration = 1f;
        [SerializeField] private TMPro.TMP_Text warningText;
        [SerializeField]
        private string[] warningTexts = new string[]
        {
            "It's getting dark...",
            "Hurry Home!"
        };
        [SerializeField] private float showWarningForSeconds = 3.5f;
        public UnityEngine.Events.UnityEvent onDayTriggered = new UnityEngine.Events.UnityEvent();
        public UnityEngine.Events.UnityEvent onNightTriggered = new UnityEngine.Events.UnityEvent();

        [Header("References")]
        [SerializeField] private PlayerSelectionUI playerSelectionUI;
        public PlayerSelectionUI PlayerSelectionUI => playerSelectionUI;
        private HomeZone homeZone;
        [SerializeField] private GameObject endScreen;
        [SerializeField] private GameObject inGameUI;
        [SerializeField] private GameObject runHomeUI;
        [SerializeField] private EndStage endStage;
        [HideInInspector] public SpawnPoint[] spawnPoints;
        
        List<PlayerController> allPlayers = new List<PlayerController>();
        List<PlayerController> dangeredPlayers = new List<PlayerController>();
        List<PointsGroup> pointGroups = new List<PointsGroup>();

        [SerializeField] private GameObject spawnPFX;
        
        GameState currentState = GameState.Startup;

        private bool shouldLockPlayers = false;
        public bool ShouldLockPlayers => shouldLockPlayers;

        public float PlayerCount => allPlayers.Count;

        private bool isNight = false;
        public bool IsNight => isNight;
        
        List<SpriteRenderer> spawnedSafeIndicators = new List<SpriteRenderer>();
        private int usedSafeSpaces = 0;

        private CameraController cameraController;

        

        CameraShake shake;

        Dictionary<PlayerController, int> PlayerScores = new Dictionary<PlayerController, int>();


        public AudioClip dayAudio, nightAudio, winSting;

        public PlayerDefinitionsForGame playerDefinitionsForGame;
        
        private void Awake()
        {
            spawnPoints = FindObjectsOfType<SpawnPoint>();
            
            if(!homeZone)
                homeZone = FindObjectOfType<HomeZone>();

            endStage = FindObjectOfType<EndStage>();

            cameraController = FindObjectOfType<CameraController>();
            shake = FindObjectOfType<CameraShake>();
            playerDefinitionsForGame = Resources.Load<PlayerDefinitionsForGame>("PlayerDefinitionsForGame");
            instance = this;
        }


        private void OnDestroy()
        {
            Timing.KillCoroutines("GM");
        }


        private void Start()
        {
            pointGroups = FindObjectsOfType<PointsGroup>().ToList();
            AudioManager.Instance.PlayNewBackgroundAudio(nightAudio, 0f);
            endStage.gameObject.SetActive(false);
            GoToState(GameState.Startup);
        }

        public void PlayGame()
        {
            GoToState(GameState.InGame);
        }

        void GoToState(GameState newState)
        {
            Debug.Log("Going to new game state: " + newState);

            switch (newState)
            {
                case GameState.Startup:
                    Timing.RunCoroutine(InitialGameStart(), "GM");
                    inGameUI.SetActive(false);
                    endScreen.SetActive(false);
                    break;
                case GameState.InGame:
                    cameraController.ResetToGameCameraTarget();
                    Timing.RunCoroutine(StartRound(true), "GM");
                    inGameUI.SetActive(true);
                    endScreen.SetActive(false);
                    break;
                case GameState.GameOver:
                    FadeToBlack.Instance.FadeIn();
                    FindWinner();                    
                    inGameUI.SetActive(false);
                    endScreen.SetActive(true);
                    break;
            }
            currentState = newState;
        }

        public void GoToMenu()
        {
                        
            var players = FindObjectsOfType<PlayerController>();
            foreach(var pc in players)
            {
                Destroy(pc.gameObject);
            }
            
            allPlayers.Clear();
            dangeredPlayers.Clear();
            LoadingManager.Instance.LoadScene("Menu");
        }

        public void StartGameFromMenuPressed()
        {
            GoToState(GameState.Startup);
        }

        public void StartPlayerSelection()
        {
            playerSelectionUI.gameObject.SetActive(true);
        }

        void SpawnPlayers()
        {
            shouldLockPlayers = true;
            playerSelectionUI.gameObject.SetActive(false);

            if (Time.time > 10)
            {
                for(int i = 1; i <  Mathf.Min(ReInput.controllers.joystickCount, 3) + 1; ++i)
                {
                    var player = ReInput.players.GetPlayer(i);
                    player.controllers.AddController(ReInput.controllers.Joysticks[i-1], true);
                    player.controllers.maps.LoadMap(ControllerType.Joystick, 0, "Default", "Default", true);
                }

                var kplayer = ReInput.players.GetPlayer(0);
                kplayer.controllers.hasKeyboard = true;
                kplayer.controllers.maps.LoadMap(ControllerType.Keyboard, 0, "Default", "Default", true);
            }
            var players = Time.time > 10 ? playerDefinitionsForGame.currentPlayers : (playerDefinitionsForGame.testPlayers).Take(Mathf.Min(ReInput.controllers.joystickCount, 3) + 1).ToArray();
            int currentSpawnPoint = 0;

            PlayerScores.Clear();

            for (int i = 0; i < players.Length; ++i)
            {
                var spawnedPlayer = Instantiate(playerPrefab, spawnPoints[currentSpawnPoint].transform.position, spawnPoints[currentSpawnPoint].transform.rotation);
                Instantiate(spawnPFX, spawnPoints[currentSpawnPoint].transform.position, spawnPoints[currentSpawnPoint].transform.rotation * Quaternion.Euler(-90f, 0f, 0f));
                var pc = spawnedPlayer.GetComponent<PlayerController>();
                PlayerScores.Add(pc, 0);
                pc.SetPlayerID(ReInput.players.GetPlayer(i));
                pc.SetDetails(players[i]);
                allPlayers.Add(pc);
                var so = Instantiate(scoreTextPrefab, scoreTextContainer);
                var text = so.GetComponentInChildren<TMPro.TMP_Text>();
                var pt = text.GetComponent<PulseText>();
                so.GetComponent<PlayerFrameScore>().SetBackground(players[i].backgroundSprite);
                pc.ScoreUI = so.GetComponent<PlayerFrameScore>();
                pc.onScoreChanged.AddListener(t => { text.text = t.ToString(); });
                pc.onLoseScore.AddListener(() => pt.Pulse());
                
                currentSpawnPoint++;
                if (currentSpawnPoint >= spawnPoints.Length)
                    currentSpawnPoint = 0;
            }
        }

        IEnumerator<float> InitialGameStart()
        {
            SpawnPlayers();

            yield return Timing.WaitForSeconds(3f);
            
            GoToState(GameState.InGame);
        }

        IEnumerator<float> StartRound(bool firstRound = false)
        {
            shouldLockPlayers = true;
            
            if(!firstRound)
            {
                foreach (var player in allPlayers)
                {
                    player.AutoTraverseToSpawn();
                }

                yield return Timing.WaitForOneFrame;
                
                while (allPlayers.Any(x => x.IsAIController))
                    yield return Timing.WaitForOneFrame;
            }

            
            currentRound++;
            roundDuration = currentRound == 1 ? initialRoundTime : Random.Range(minRoundTime, maxRoundTime);
            float timer = 0f;
            while (timer < secondsBeforeRoundStarts)
            {
                timer += Time.deltaTime;
                yield return Timing.WaitForOneFrame;
            }

            Timing.RunCoroutine(ChangeTOD(true), "GM");
            homeZone.ReleasePlayers();
            shouldLockPlayers = false;
            spawnedSafeIndicators.Clear();
            usedSafeSpaces = 0;
            //foreach (Transform child in safeSpaceTransform)
            //{
            //    Destroy(child.gameObject);
            //}
            
            foreach (PlayerController player in allPlayers)
            {
                player.StopNightVibration();
                if (firstRound)
                    player.TeleportToSpawn();
                else
                    player.AutoTraverseToSpawn();
                player.SoftVibrate();
            }

            yield return Timing.WaitForOneFrame;
            
            while (allPlayers.Any(x => x.IsAIController))
                yield return Timing.WaitForOneFrame;
            
            foreach (var group in pointGroups)
                group.SpawnInitalPoint();
            Timing.RunCoroutine(RoundTimer(), "GM");
        }

        IEnumerator<float> ShowWarnings()
        {
            runHomeUI.SetActive(true);
            for (int i = 0; i < warningTexts.Length; ++i)
            {
                if(warningText)
                    warningText.text = warningTexts[i];
                yield return Timing.WaitForSeconds(showWarningForSeconds);
            }
            runHomeUI.SetActive(false);
        }

        IEnumerator<float> ChangeTOD(bool toDay)
        {
            float timer = 0f;

            if (toDay)
            {
                AudioManager.Instance.PlayNewBackgroundAudio(dayAudio, 1.5f);
            }
            else
            {
                lightening.Flash();
                AudioManager.Instance.PlayNewBackgroundAudio(nightAudio, 1.5f);
            }

            Color startColor = sun.color;
            float startIntensity = sun.intensity;
            while (timer < dayNightTransitionDuration)
            {
                timer += Time.deltaTime;
                sun.color = Color.Lerp(startColor, toDay ? dayColor : nightColor, timer / dayNightTransitionDuration);
                sun.intensity = Mathf.Lerp(startIntensity, toDay ? dayIntensity : nightIntensity, timer / dayNightTransitionDuration);
                yield return Timing.WaitForOneFrame;
            }

            isNight = !toDay;
        }

        IEnumerator<float> RoundTimer()
        {
            float timer = 0f;
            shouldLockPlayers = false;
            while (timer < roundDuration)
            {
                timer += Time.deltaTime;
                yield return Timing.WaitForOneFrame;
            }

            foreach (var group in pointGroups)
                group.HideAllPoints();

            int safeSpaces = Mathf.Max(allPlayers.Count - currentRound, 1);

            //Vector3 start = safeSpaceTransform.position -
            //                safeSpaceTransform.right * (safeSpaces - 1) * (seperationDistance + safeIndicatorSize);

            
//            for (int i = 0; i < spawnPoints.Length; ++i)
//            {
//                var sr = spawnPoints[i].GetComponentInChildren<SpriteRenderer>();
//                sr.color = safeSpaceUnUsedColor;
//                spawnedSafeIndicators.Add(sr);
//            }
            usedSafeSpaces = 0;
            
            Timing.RunCoroutine(ChangeTOD(false), "GM");
            homeZone.UnlockZone();
            timer = 0f;

            Timing.RunCoroutine(ShowWarnings(), "GM");

            foreach (var player in allPlayers)
                player.VibrateForPointLoss();

            shake.Shake();

            while (timer < getHomeTime)
            {
                timer += Time.deltaTime;
                if (homeZone.PlayersInZone.Count >= Mathf.Max(allPlayers.Count - currentRound, 1))
                    break;
                yield return Timing.WaitForOneFrame;
            }
                      
            var originalDangered = dangeredPlayers = allPlayers.Except(homeZone.PlayersInZone).ToList();
            foreach (PlayerController controller in originalDangered)
            {
                controller.StartVibrationForNight();
            }
            while(homeZone.PlayersInZone.Count < Mathf.Max(allPlayers.Count - currentRound, 1))
            {
                yield return Timing.WaitForSeconds(outOfZoneNightTickDuration);
                dangeredPlayers = allPlayers.Except(homeZone.PlayersInZone).ToList();
                foreach(var player in dangeredPlayers)
                {
                    player.RemovePoints(outOfZoneNightPenaltyPerTick);
                }

                var stopVibPlayers = originalDangered.Except(dangeredPlayers);
                foreach (PlayerController player in stopVibPlayers)
                {
                    player.StopNightVibration();
                }
            }

            dangeredPlayers = allPlayers.Except(homeZone.PlayersInZone).ToList();
            foreach (var player in dangeredPlayers)
            {
                player.RemovePoints(lostPlayersPointReductionPerWave[currentRound-1]);
            }
            var stopVibPlayersLate = originalDangered.Except(dangeredPlayers);
            foreach (PlayerController player in stopVibPlayersLate)
            {
                player.StopNightVibration();
            }

            runHomeUI.SetActive(false);

            if (currentRound < maxRounds)
            {
                Timing.RunCoroutine(StartRound(), "GM");
            }
            else
            {
                GoToState(GameState.GameOver);
            }
        }

        void FindWinner() 
        {
            
            Timing.RunCoroutine(ChangeTOD(true), "GM");
            Timing.RunCoroutine(FindWinnerPost());
        }

        IEnumerator<float> FindWinnerPost()
        {
            yield return Timing.WaitForSeconds(2.5f);
            AudioManager.Instance.PlayNewBackgroundAudio(winSting, 1f);
            shouldLockPlayers = true;
            var orderedPlayers = allPlayers.OrderByDescending(x => x.CurrentPoints).ToList();
            orderedPlayers[0].Win();
            cameraController.GoToNewTarget(orderedPlayers[0].transform);
            shake.enabled = false;
            for (int i = 1; i < orderedPlayers.Count; ++i)
            {
                orderedPlayers[i].Lose();
            }
            //endScreen.GetComponent<EndScreen>().SetWinner(orderedPlayers[0]);

            var scores = orderedPlayers.Select(x => x.CurrentPoints).ToArray();
            var ids = orderedPlayers.Select(x => x.PlayerID).ToArray();
                        
            endStage.PopulateAndShow(ref orderedPlayers, ref scores);
            Camera.main.gameObject.SetActive(false);
            endScreen.SetActive(true);

            FadeToBlack.Instance.FadeOut();
        }

        public void ResetGame()
        {
            int currentSpawnPoint = 0;
            shouldLockPlayers = false;
            for (int i = 0; i < allPlayers.Count; i++)
            {
                PlayerController players = (PlayerController)allPlayers[i];
                players.ResetPlayer(spawnPoints[currentSpawnPoint].transform);
                    currentSpawnPoint++;
                if (currentSpawnPoint >= spawnPoints.Length)
                    currentSpawnPoint = 0;
            }
            
            currentRound = 0;
            GoToState(GameState.InGame);
        }

        public void SetNewUsedSpace()
        {
            usedSafeSpaces++;
//            if (spawnedSafeIndicators.Count > usedSafeSpaces - 1)
//            {
//                spawnedSafeIndicators[usedSafeSpaces - 1].color = safeSpaceUsedColor;
//            }
        }
        public void SetPlayerScore(PlayerController player)
        {
            PlayerScores[player] = player.CurrentPoints;
            var ops = PlayerScores.OrderByDescending(x => x.Value).ToList();
            foreach (var o in ops)
            {
                o.Key.ScoreUI.SetEmotion(o.Value == 0 ? EmotionType.Danger : EmotionType.Default);
            }
            ops[0].Key.ScoreUI.SetEmotion(ops[0].Value == 0 ? EmotionType.Danger : EmotionType.Winner);
            ops.Last().Key.ScoreUI.SetEmotion(ops.Last().Value==0?EmotionType.Danger: EmotionType.Losing);

        }
    }
}
