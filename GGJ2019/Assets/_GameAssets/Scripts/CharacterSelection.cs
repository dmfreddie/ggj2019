﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MEC;
using UnityEngine;

namespace GGJ
{

	public class CharacterSelection : MonoBehaviour
	{
		[SerializeField] private GameObject dummyPlayerPrefab;
		[SerializeField] private Transform[] spawnPoints;
		[SerializeField] private Material offRingColor, onRingColor;
		[SerializeField] private List<PlayerDetails> availablePlayerDetails = new List<PlayerDetails>();
		[HideInInspector] public List<PlayerDetails> players = new List<PlayerDetails>();
		[SerializeField] private MeshRenderer[] targetRenderers;
		List<DummyPlayer> dummyPlayers = new List<DummyPlayer>();

		private PlayerDefinitionsForGame playerDefinitionsForGame;
		private SceneDefinitions sceneDefinitions;

		private bool starting = false;
		
		void Start()
		{
			playerDefinitionsForGame = Resources.Load<PlayerDefinitionsForGame>("PlayerDefinitionsForGame");
			sceneDefinitions = Resources.Load<SceneDefinitions>("SceneDefinitions");
		}
		
		public void AddCharacter(int playerID)
		{
			var targetDetail = availablePlayerDetails[0];
			players.Add(targetDetail);
			availablePlayerDetails.Remove(targetDetail);
			targetRenderers[playerID].sharedMaterial = onRingColor;
			targetRenderers[playerID].transform.GetChild(0).gameObject.SetActive(true);
			var dp = Instantiate(dummyPlayerPrefab, spawnPoints[playerID].position, Quaternion.identity).GetComponent<DummyPlayer>();
			dp.Init(this, playerID);
			dp.SetDetails(players[playerID], targetRenderers[playerID]);
			dummyPlayers.Add(dp);
		}

		public void RemoveCharacter(int playerID)
		{
			throw new NotSupportedException("Currently not supported");
			return;
			var old = dummyPlayers[playerID].currentDetails;
			availablePlayerDetails.Add(old);
			players.Remove(old);
			dummyPlayers.Remove(dummyPlayers[playerID]);
			targetRenderers[playerID].sharedMaterial = offRingColor;
		}

		public void ChangeColorScheme(int playerID)
		{
			if (availablePlayerDetails.Count > 0)
			{
				var targetDetail = availablePlayerDetails[0];
				var old = dummyPlayers[playerID].currentDetails;
				dummyPlayers[playerID].SetDetails(targetDetail, targetRenderers[playerID]);
				availablePlayerDetails.Remove(targetDetail);
				players[playerID] = targetDetail;
				availablePlayerDetails.Add(old);
			}
		}

		public void CheckReady()
		{
			if (dummyPlayers.All(x => x.Ready) && !starting)
			{
				Timing.RunCoroutine(WaitToStartGame());
			}
		}

		public IEnumerator<float> WaitToStartGame()
		{
			starting = true;
			
			yield return Timing.WaitForSeconds(3);

			if (dummyPlayers.All(x => x.Ready))
			{
				foreach (var x in dummyPlayers) x.SetPlayable();
				playerDefinitionsForGame.currentPlayers = players.ToArray();
				LoadingManager.Instance.LoadScene(sceneDefinitions.GetRandomScene());
			}
			else
			{
				starting = false;
			}
		}
		
	}

}