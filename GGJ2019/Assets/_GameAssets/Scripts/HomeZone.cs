﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace GGJ
{
    [SelectionBase]
    public class HomeZone : MonoBehaviour
    {
        [SerializeField] private int pointReductionPerTick = 1;
        [SerializeField] private float initialDelay = 5f;
        [SerializeField] private float tickDelay = 1.5f;



        [Header("References")]
        [SerializeField] private Color lockedColor = new Color(0.75f, 0.1f, 0.1f, 0.85f);
        [SerializeField] private Color unlockedColor = new Color(0.1f, 0.75f, 0.1f, 0.85f);
        //[SerializeField] private SpriteRenderer lockedIndicator;
        [SerializeField] private Transform edgeRednerer;
        [SerializeField] private ParticleSystem edgeParticles;
        [SerializeField] private MeshRenderer[] homeZoneRenderers;
        
        List<PlayerController> playersInZone = new List<PlayerController>();
        public List<PlayerController> PlayersInZone => playersInZone;

        bool zoneLocked = false;
        bool firstPass = true;

        private Vector3 fullEdgeRendererSize;
        
        CoroutineHandle handle;

        private void Start()
        {
            fullEdgeRendererSize = edgeRednerer.localScale;   
            handle = Timing.RunCoroutine(LosePointsOverTime());
            UnlockZone();
        }

        private void OnDestroy()
        {
            if(handle.IsRunning)
                Timing.KillCoroutines(handle);
        }

        private void OnTriggerEnter(Collider other)
        {
            var pc = other.GetComponent<PlayerController>();
            if(pc)
            {
                playersInZone.Add(pc);
                GameManager.Instance.SetNewUsedSpace();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var pc = other.GetComponent<PlayerController>();
            if (pc)
            {
                playersInZone.Remove(pc);
            }
        }

        public void ReleasePlayers()
        {
            zoneLocked = true;
            firstPass = true;
            
        }

        public void UnlockZone()
        {
            zoneLocked = false;
            
            Timing.RunCoroutine(LerpColor(lockedColor, unlockedColor));
            Timing.RunCoroutine(ScaleZone(new Vector3(fullEdgeRendererSize.x, fullEdgeRendererSize.y, fullEdgeRendererSize.z / 90f), fullEdgeRendererSize));
            edgeRednerer.localScale = fullEdgeRendererSize;
            edgeParticles.Play();
        }

        IEnumerator<float> LosePointsOverTime()
        {
            while(true)
            {
                if(zoneLocked)
                {
                    if(firstPass)
                    {
                        Timing.RunCoroutine(LerpColor(lockedColor, unlockedColor, 0.1f));
                        
                        Timing.RunCoroutine(ScaleZone(new Vector3(fullEdgeRendererSize.x, fullEdgeRendererSize.y, fullEdgeRendererSize.z / 90f), fullEdgeRendererSize, 0.1f));
                        
                        yield return Timing.WaitForSeconds(initialDelay);
                        firstPass = false;
                        
                        Timing.RunCoroutine(LerpColor(unlockedColor, lockedColor));
                        Timing.RunCoroutine(ScaleZone(fullEdgeRendererSize, new Vector3(fullEdgeRendererSize.x, fullEdgeRendererSize.y, fullEdgeRendererSize.z / 90f)));
                        
                        edgeParticles.Stop();
                    }
                    for(int i = 0; i < playersInZone.Count; ++i)
                    {
                        playersInZone[i].RemovePoints(pointReductionPerTick);
                    }
                    yield return Timing.WaitForSeconds(tickDelay);
                }

                yield return Timing.WaitForOneFrame;
            }
        }

        IEnumerator<float> LerpColor(Color start, Color end, float overrideDuration = 0f)
        {
            float timer = 0f;
            float duration = Math.Abs(overrideDuration) < 0.001f ? 0.5f : overrideDuration;

            while (timer < duration)
            {
                timer += Time.deltaTime;
                
                foreach (MeshRenderer meshRenderer in homeZoneRenderers)
                {
                    meshRenderer.material.color = Color.Lerp(start, end, timer/duration);
                }

                yield return Timing.WaitForOneFrame;
            }
        }

        IEnumerator<float> ScaleZone(Vector3 start, Vector3 end, float overrideDuration = 0f)
        {
            float timer = 0f;
            float duration = Math.Abs(overrideDuration) < 0.001f ? 0.5f : overrideDuration;

            while (timer < duration)
            {
                timer += Time.deltaTime;
                
                edgeRednerer.localScale = Vector3.Lerp(start, end, timer / duration);

                yield return Timing.WaitForOneFrame;
            }
        }

    }

}