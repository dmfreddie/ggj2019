﻿using UnityEngine;

namespace GGJ
{
    [CreateAssetMenu(menuName ="GGJ/PlayerDetails")]
    public class PlayerDetails : ScriptableObject
    {
        public Color playerColor;
        public Material playerMaterial;
        public Sprite backgroundSprite;
        public Sprite playerSprite;
        public string playerName;
    }

}