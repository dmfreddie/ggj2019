﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    public class ScaleOverTime : MonoBehaviour
    {


        [SerializeField] private Vector3 startLocalScale, endLocalScale;
        [SerializeField] private AnimationCurve scaleAniamtion;
        float duration;

        float timer = 0f;

        private void Start()
        {
            duration = scaleAniamtion.keys[scaleAniamtion.length - 1].time;
        }

        private void OnEnable()
        {
            timer = 0f;
        }

        private void Update()
        {
            timer += Time.deltaTime;

            transform.localScale = Vector3.Lerp(startLocalScale, endLocalScale, scaleAniamtion.Evaluate(Mathf.PingPong(timer, duration)));
        }
    }

}