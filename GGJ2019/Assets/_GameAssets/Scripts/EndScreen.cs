﻿using UnityEngine;

namespace GGJ
{

    public class EndScreen : MonoBehaviour
    {
         
        public void GoToMenu()
        {
            GameManager.Instance.GoToMenu();
        }

        public void ReplayGame()
        {
            // This should be go to new random game
            LoadingManager.Instance.LoadScene(Resources.Load<SceneDefinitions>("SceneDefinitions").GetRandomScene());
        }

        public void QuitGame()
        {
            Application.Quit();
        }
        
    }

}







