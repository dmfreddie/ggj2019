﻿using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    public class AudioManager : MonoBehaviour
    {

        private static AudioManager instance;
        public static AudioManager Instance
        {
            get
            {
                if (!instance)
                    instance = FindObjectOfType<AudioManager>();
                if (!instance)
                    instance = new GameObject("AudioManager").AddComponent<AudioManager>();
                return instance;
            }
        }

        Stack<AudioSource> sources = new Stack<AudioSource>();
        List<AudioSource> playingSources = new List<AudioSource>();
        [SerializeField] private float fadeDuration = 1.5f;

        private void Awake()
        {
            if (!instance)
                DontDestroyOnLoad(this);
            else
                Destroy(gameObject);
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void Init()
        {
            // force create the instance
            var instance = Instance;
            for (int i = 0; i < 10; ++i)
            {
                var _as = new GameObject("Background Audio Source").AddComponent<AudioSource>();
                _as.transform.SetParent(instance.transform);
                instance.sources.Push(_as);
            }            
        }

        //public void Play(AudioClip newBackgroundClip, bool immediate = true)
        //{
        //    if (immediate)
        //    {
        //        playingSources.ForEach(x =>
        //        {
        //            x.Stop();
        //            sources.Push(x);
        //        });
        //        playingSources.Clear();
        //        var source = sources.Pop();
        //        source.clip = newBackgroundClip;
        //        source.loop = true;
        //        source.Play();
        //        playingSources.Add(source);
        //    }
        //    else
        //    {
        //        MEC.Timing.RunCoroutine(FadeBackgroundClips(newBackgroundClip));
        //    }
        //}

        //IEnumerator<float> FadeBackgroundClips(AudioClip newBackgroundClip)
        //{
        //    var source = sources.Pop();
        //    source.clip = newBackgroundClip;
        //    source.loop = true;
        //    source.volume = 0f;
        //    source.Play();

        //    float timer = 0f;

        //    while(timer < fadeDuration)
        //    {
        //        source.volume = Mathf.Lerp(0, 1, timer / fadeDuration);
        //        playingSources.ForEach(x => { x.volume = Mathf.Lerp(1, 0, timer / fadeDuration); });
        //        yield return MEC.Timing.WaitForOneFrame;
        //    }

        //    playingSources.ForEach(x =>
        //    {
        //        x.Stop();
        //        sources.Push(x);
        //    });
        //    playingSources.Clear();
        //    playingSources.Add(source);
        //}



        AudioSource currentBackgroundSource;

        public void PlayNewBackgroundAudio(AudioClip clip, float fadeDuration)
        {
            var xform = new GameObject($"[BGM] {clip.name}").transform;
            xform.SetParent(transform);
            var newAudioSource = xform.gameObject.AddComponent<AudioSource>();
            newAudioSource.loop = true;
            MEC.Timing.RunCoroutine(InternalBackgroundTransition(currentBackgroundSource, newAudioSource, fadeDuration));
        }

        IEnumerator<float> InternalBackgroundTransition(AudioSource oldSource, AudioSource newSource, float fadeDuration)
        {
            newSource.Play();
            if (Mathf.Approximately(fadeDuration, 0f))
            {
                newSource.volume = 1f;
                yield return MEC.Timing.WaitForOneFrame;
            }
            else
            {
                float timer = 0f;
                while(timer < fadeDuration)
                {
                    timer += Time.deltaTime;
                    if(oldSource)
                        oldSource.volume = Mathf.Lerp(1f, 0f, timer / fadeDuration);
                    newSource.volume = Mathf.Lerp(0f, 1f, timer / fadeDuration);
                    yield return MEC.Timing.WaitForOneFrame;
                }
            }
            currentBackgroundSource = newSource;
            if(oldSource)
            {
                oldSource.Stop();
                Destroy(oldSource.gameObject);
            }
        }


    }

}