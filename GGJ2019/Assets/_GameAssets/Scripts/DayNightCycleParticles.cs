﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{
    [RequireComponent(typeof(ParticleSystem))]
    public class DayNightCycleParticles : MonoBehaviour
    {

        ParticleSystem ps;
        [SerializeField] private bool activatedInDaytime;

        private void Awake()
        {
            ps = GetComponent<ParticleSystem>();
        }

        // Use this for initialization
        void Start()
        {
            GameManager.Instance.onDayTriggered.AddListener(() => {
                if (activatedInDaytime)
                    ps.Play(true);
                else
                    ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            });

            GameManager.Instance.onNightTriggered.AddListener(() => {
                if (!activatedInDaytime)
                    ps.Play(true);
                else
                    ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            });
        }
        
    }

}