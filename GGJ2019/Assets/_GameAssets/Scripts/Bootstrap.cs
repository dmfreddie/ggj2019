﻿using UnityEngine;
using UnityEngine.SceneManagement;


namespace GGJ
{

    public class Bootstrap : MonoBehaviour
    {

        const string mainMenuScene = "Menu";
        const string mainGameScene = "Main";
        const string devScene = "_DevBootstrap";

        //#if UNITY_EDITOR
        //        [UnityEditor.MenuItem("Global Game Jam/Play With Menu")]
        //        static void EnableFromMenu()
        //        {
        //            UnityEditor.EditorPrefs.SetBool("DEBUG_FROM_MENU", true);
        //        }
        //
        //        [UnityEditor.MenuItem("Global Game Jam/Play With Menu", validate = true)]
        //        static bool EnableFromMenuValidate()
        //        {
        //            if (UnityEditor.EditorPrefs.HasKey("DEBUG_FROM_MENU"))
        //            {
        //                var fromMenu = UnityEditor.EditorPrefs.GetBool("DEBUG_FROM_MENU");
        //                return !fromMenu;
        //            }
        //            return true;
        //        }
        //
        //        [UnityEditor.MenuItem("Global Game Jam/Play Without Menu")]
        //        static void DisableFromMenu()
        //        {
        //            UnityEditor.EditorPrefs.SetBool("DEBUG_FROM_MENU", false);
        //        }
        //
        //        [UnityEditor.MenuItem("Global Game Jam/Play Without Menu", validate = true)]
        //        static bool DisableFromMenuValidate()
        //        {
        //            if (UnityEditor.EditorPrefs.HasKey("DEBUG_FROM_MENU"))
        //            {
        //                var fromMenu = UnityEditor.EditorPrefs.GetBool("DEBUG_FROM_MENU");
        //                return fromMenu;
        //            }
        //            return false;
        //        }
        //#endif

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        static void Init()
        {
#if UNITY_EDITOR
            if (SceneManager.GetActiveScene().name != "Menu")
                SceneManager.LoadScene(devScene, LoadSceneMode.Additive);
            //bool playFromMenu = UnityEditor.EditorPrefs.GetBool("DEBUG_FROM_MENU");
            //if (playFromMenu)
            //{
            //    //SceneManager.LoadScene(mainGameScene, LoadSceneMode.Single);
            //    SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Single);

            //    //FindObjectOfType<GameManager>().SetInitialState(GameState.Menu);
            //}
            //else
            //{
            //    SceneManager.LoadScene(mainGameScene, LoadSceneMode.Single);
            //    //FindObjectOfType<GameManager>().SetInitialState(GameState.CollectPlayers);
            //}
#else
            SceneManager.LoadScene(mainGameScene, LoadSceneMode.Single);
            SceneManager.LoadScene(mainMenuScene, LoadSceneMode.Additive);      

            FindObjectOfType<GameManager>().SetInitialState(GameState.Menu);
#endif
            //        }

        }



    }
}