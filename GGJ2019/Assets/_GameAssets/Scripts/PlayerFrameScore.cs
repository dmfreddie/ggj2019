﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ
{ // BETH DID THIS YAY! < With Help 

    public enum EmotionType
    {
        Default,
        Losing,
        Danger,
        Winner
    }
    public class PlayerFrameScore : MonoBehaviour
    {

        [SerializeField] private Image PlayersFace;
        [SerializeField] private Image Background;
        [SerializeField] private Sprite[] Emotions;
        [SerializeField] private Color dangerColor = Color.red, normalColor = Color.white, winningColor = Color.green;
        [SerializeField] private TMPro.TMP_Text scoreText;

//        public void SetColour (Color PlayerColour, int playerID)
//        {
//            FillColourBackground.color = PlayerColour;
//            FillColourScoreBackground.color = PlayerColour;
//            for (int i = 0; i < Backgrounds.Length; ++i)
//                Backgrounds[i].SetActive(false);
//            Backgrounds[playerID].SetActive(true);
//        }

        public void SetBackground(Sprite backgroundSprite)
        {
            Background.sprite = backgroundSprite;
        }
        
        public void SetEmotion(EmotionType etype)
        {
            switch (etype)
            {
                case EmotionType.Default:
                    PlayersFace.sprite = Emotions[0];
                    scoreText.color = normalColor;
                    break;
                case EmotionType.Losing:
                    PlayersFace.sprite = Emotions[1];
                    scoreText.color = normalColor;
                    break;
                case EmotionType.Danger:
                    PlayersFace.sprite = Emotions[2];
                    scoreText.color = dangerColor;
                    break;
                case EmotionType.Winner:
                    PlayersFace.sprite = Emotions[3];
                    scoreText.color = winningColor;
                    break;
            }
        }

        public void SetScore(int score)
        {
            scoreText.text = score.ToString();
        }
    }
}