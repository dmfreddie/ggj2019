﻿using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    public class Lightening : MonoBehaviour
    {
        [SerializeField] private AnimationCurve lighteningIntensity;
        [SerializeField] private Light lightningLight;
        float duration;
        public AudioSource lightnigAudio;
        public float audioDelay = 1f;

        // Use this for initialization
        void Start()
        {
            duration = lighteningIntensity.keys[lighteningIntensity.length - 1].time;
        }

        public void Flash()
        {
            Timing.RunCoroutine(FlashInternal());
        }

        IEnumerator<float> FlashInternal()
        {
            lightnigAudio.Play();
            yield return Timing.WaitForSeconds(audioDelay);
            float timer = 0f;
            while(timer < duration)
            {
                timer += Time.deltaTime;
                lightningLight.intensity = lighteningIntensity.Evaluate(timer);
                yield return Timing.WaitForOneFrame;
            }
        }
    }

}