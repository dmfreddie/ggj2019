﻿using UnityEngine;

namespace GGJ
{
    public class PlayerSelectionUI : MonoBehaviour
    {
        [SerializeField] private GameObject presstoJoin;
        [SerializeField] private GameObject pressToStart;
        [SerializeField] private GameObject connectedPlayerTextPrefab;
        [SerializeField] private Transform connectedPlayersContainer;

        private void Start()
        {
            foreach (Transform child in connectedPlayersContainer.transform)
            {
                Destroy(child.gameObject);
            }
            pressToStart.SetActive(false);
            presstoJoin.SetActive(true);
        }

        public void AddNewPlayer(int ID)
        {
            var so = Instantiate(connectedPlayerTextPrefab, connectedPlayersContainer);
            var text = so.GetComponentInChildren<TMPro.TMP_Text>();
            so.GetComponent<PlayerFrameScore>().SetBackground(GameManager.Instance.playerDefinitionsForGame.currentPlayers[ID-1].backgroundSprite);
            text.text = "Player " + ID;

            if(connectedPlayersContainer.childCount >= 2)
            {
                pressToStart.SetActive(true);
            }

            if (connectedPlayersContainer.childCount >= 8)
            {
                presstoJoin.SetActive(false);
            }
        }
    }
}
