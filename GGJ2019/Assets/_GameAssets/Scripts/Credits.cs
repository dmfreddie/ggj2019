﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace GGJ
{

    [RequireComponent(typeof(TMPro.TMP_Text))]
    public class Credits : MonoBehaviour
    {
        [SerializeField] string[] names = new string[]
        {
            "Beth Reed",
            "Christian Maund",
            "Helen Simm",
            "Frederic Babord"
        };

        TMPro.TMP_Text creditText;

        private void Awake()
        {
            creditText = GetComponent<TMPro.TMP_Text>();
        }
        
        void OnEnable()
        {
            creditText.text = "BY" + System.Environment.NewLine;
            var people = names.ToList().OrderBy(x => Random.value).ToList();
            for (int i = 0; i < people.Count; i++)
            {
                if (i > 0)
                    creditText.text += ", ";
                creditText.text += people[i];
            }
        }
        
    }

}