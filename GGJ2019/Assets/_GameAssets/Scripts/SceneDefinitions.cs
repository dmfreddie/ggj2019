using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GGJ
{
    [CreateAssetMenu(menuName = "GGJ/SceneDefinitions", fileName = "SceneDefinitions")]
    public class SceneDefinitions : ScriptableObject
    {
        [HideInInspector] public string[] sceneNames;
        
#if UNITY_EDITOR
        [SerializeField] private SceneAsset mainMenuScene;
        [SerializeField] private SceneAsset characterSelectionScene;
        [SerializeField] private SceneAsset[] targetScenes;
        [SerializeField] private SceneAsset devScene;
#endif

        public string GetRandomScene()
        {
            return sceneNames[Random.Range(0, sceneNames.Length)];
        }
        

        private void OnValidate()
        {
#if UNITY_EDITOR
            sceneNames = new string[targetScenes.Length];
            for (var i = 0; i < targetScenes.Length; i++)
            {
                sceneNames[i] = targetScenes[i].name;
            }
            List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
            
            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(mainMenuScene), true));
            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(characterSelectionScene), true));
            
            foreach (var sceneAsset in targetScenes)
            {
                string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
                if (!string.IsNullOrEmpty(scenePath))
                    editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
            }

            editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(devScene), true));

            // Set the Build Settings window Scene list
            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
#endif
        }
    }
}