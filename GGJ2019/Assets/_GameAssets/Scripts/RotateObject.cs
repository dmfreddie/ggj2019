﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    public class RotateObject : MonoBehaviour
    {

        [SerializeField] private Vector3 axis = new Vector3(0f, 1f, 0f);
        [SerializeField] private float rotationSpeed = 2f;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(axis, rotationSpeed * Time.deltaTime);
        }
    }


}