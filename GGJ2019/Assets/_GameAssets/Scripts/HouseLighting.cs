﻿using UnityEngine;

namespace GGJ
{

    public class HouseLighting : MonoBehaviour {

        public GameObject houseLighting;
        
        void Start() {
            GameManager.Instance.onDayTriggered.AddListener(() => houseLighting.SetActive(false));
            GameManager.Instance.onNightTriggered.AddListener(() => houseLighting.SetActive(true));
        }
        
    }

}