﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System;
using System.Linq;
using MEC;
using UnityEngine.AI;
using UnityEngine.Events;

namespace GGJ
{

    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }

    public class PlayerController : MonoBehaviour
    {
        [SerializeField]  int playerID = 0;

        [Header("Movement")]
        [SerializeField] private float movementSpeed = 5f;
        [SerializeField] private float nightSpeedIncrease = 1.5f;
        private float nightSpeed;
        [Range(0f, 0.1f)][SerializeField] private float rotationSpeed = 0.05f;
        [SerializeField] private bool invertHorizontal;
        [SerializeField] private bool invertVertical;

        [Header("Controller Vibration")] 
        [SerializeField] private float pointLossVibration = 0.8f;
        [SerializeField] private float pointLossVibrationDuration = 0.8f;
        [SerializeField] private float nightModeVibration = 0.4f;
        
        [Header("References")]
        [SerializeField] private Animator animator;
        [SerializeField] private CharacterController characterController;
        [SerializeField] private Renderer playerRenderer;
        [SerializeField] private NavMeshAgent navAgent;
        [SerializeField] private PlayerInteractionTrigger playerInteractionTriger;

        public PlayerFrameScore ScoreUI;

        public AudioSource footsteps;

        public Renderer ring;

        [Header("Knockback")]
        [SerializeField] private float knockbackDistance = 0.5f;
        [SerializeField] private float knockbackDelay = 1f;
        float timeAtLastKnockBack = -10000f;


        Player input;
        int currentPoints = 0;
        public int CurrentPoints => currentPoints;
        int animatorSpeedParameter = Animator.StringToHash("Speed");
        int animatorWinParameter = Animator.StringToHash("Win");
        int animatorLoseParameter = Animator.StringToHash("Lose");
        int animatorResetParameter = Animator.StringToHash("Reset");
        int animatorJumpParameter = Animator.StringToHash("Jump");
        Vector3 targetRotation = new Vector3(0f, 0f, 1f);
        Renderer[] renderers;

        public FloatEvent onScoreChanged = new FloatEvent();
        public UnityEvent onLoseScore = new UnityEvent();
        
        PlayerDetails details;
        public PlayerDetails Details => details;

        internal bool LockControls => GameManager.Instance.ShouldLockPlayers;
        internal bool IsAIController => navAgent.enabled;
        
        public int PlayerID => playerID;

        Vector3 spawnPosition;
       
               
        private void Awake()
        {
            //if (GameManager.Instance)
            //    LockControls = true;
            //else
            //    SetPlayerID(ReInput.players.GetPlayer(0));

            renderers = GetComponentsInChildren<Renderer>();
        }

        private IEnumerator Start()
        {
            input = ReInput.players.GetPlayer(playerID);
            nightSpeed = movementSpeed + nightSpeedIncrease;

            GameManager.Instance.onDayTriggered.AddListener(() => SetDayMat());
            GameManager.Instance.onNightTriggered.AddListener(() => SetNightMat());

            yield return 0;
            
            spawnPosition = GameManager.Instance.spawnPoints[playerID].transform.position;
        }

        private void Update()
        {
            UpdateInput();
            UpdateHit();
            UpdateAnimation();
        }

        // HACK
        private void LateUpdate()
        {
            var pos = transform.position;
            pos.y = 0.49f;
            transform.position = pos;
        }

        void UpdateAudio()
        {
            if (Mathf.Abs(characterController.velocity.magnitude) > 0.1f && !footsteps.isPlaying)
                footsteps.Play();
            else if (Mathf.Abs(characterController.velocity.magnitude) < 0.1f && footsteps.isPlaying)
                footsteps.Stop();
        }

        void UpdateInput()
        {
            if (LockControls)
            {
                if (characterController.velocity.magnitude > 0.1f)
                {
                    var direction = characterController.velocity.normalized;
                    var currentMag = characterController.velocity.magnitude;
                    float currentVel = 0f;
                    float dampedSpeed = Mathf.SmoothDamp(currentMag, 0f, ref currentVel, 0.1f);
                    characterController.Move(direction * dampedSpeed * Time.deltaTime);
                }
                return;
            }

            var horizontal = input.GetAxis("Horizontal") * (invertHorizontal ? 1f : -1f);
            var vertical = input.GetAxis("Vertical") * (invertVertical ? 1f : -1f);

//            if(input.GetButtonDown("Jump"))
//            {
//                animator.SetTrigger(animatorJumpParameter);
//            }

            Vector3 motion = new Vector3(horizontal, 0, vertical) * (GameManager.Instance.IsNight ? nightSpeed : movementSpeed);
            if (Mathf.Abs(motion.magnitude) > 0.1f)
                targetRotation = motion;

            float angularVelocity = 0f;
            var delta = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(targetRotation));
            var t = Mathf.SmoothDampAngle(delta, 0.0f, ref angularVelocity, rotationSpeed);
            t = 1.0f - Mathf.Clamp01(t / delta);

            if(delta > 1f)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetRotation), t);
            characterController.Move(motion * Time.deltaTime);
            
        }
        
        void UpdateHit()
        {
            if(input.GetButton("Interact") && Time.time - timeAtLastKnockBack >= knockbackDelay && !playerInteractionTriger.gameObject.activeInHierarchy)
            {
                playerInteractionTriger.gameObject.SetActive(true);
                animator.SetTrigger("Attack");
            }
        }

        void UpdateAnimation()
        {
            animator.SetFloat(animatorSpeedParameter, Mathf.Clamp01(characterController.velocity.magnitude / movementSpeed));
        }

        public void AddPoints(int amount)
        {
            currentPoints += amount;
            onScoreChanged?.Invoke(currentPoints);
            GameManager.Instance.SetPlayerScore(this);
        }

        public void RemovePoints(int amount)
        {
            currentPoints -= amount;
            currentPoints = Mathf.Clamp(currentPoints, 0, int.MaxValue);
            onScoreChanged?.Invoke(currentPoints);
            VibrateForPointLoss();
            onLoseScore?.Invoke();
            if (currentPoints <= 0)
                ScoreUI.SetEmotion(EmotionType.Danger);
            GameManager.Instance.SetPlayerScore(this);
        }

        internal void SetPlayerID(Player player)
        {
            input = player;
            playerID = player.id;
        }

        internal void Win()
        {
            animator.SetTrigger(animatorWinParameter);
            transform.rotation = Quaternion.identity;
            
            //Timing.RunCoroutine(RotateToFaceCamera());
        }

        IEnumerator<float> RotateToFaceCamera()
        {
            
            var camPosOffset = Camera.main.transform.position;
            camPosOffset.y = transform.position.y;
            var target = Quaternion.LookRotation((camPosOffset - transform.position).normalized);
            transform.rotation = target;
            yield break;
            
            

            float delta = 100f;
            float angularVelocity = 0f;
            do
            {
                delta = Quaternion.Angle(transform.rotation, target);
                var t = Mathf.SmoothDampAngle(delta, 0.0f, ref angularVelocity, rotationSpeed);
                t = 1.0f - Mathf.Clamp01(t / delta);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetRotation), t);
                yield return Timing.WaitForOneFrame;
            } while (delta > 1f);
        }

        internal void Lose()
        {
            animator.SetTrigger(animatorLoseParameter);
        }


        //void OnControllerColliderHit(ControllerColliderHit hit)
        //{
        //    var angle = Quaternion.Angle(Quaternion.LookRotation(hit.normal), Quaternion.LookRotation(Vector3.up));
        //    if (angle > 10f)
        //    {
        //        var joys = input.controllers.Joysticks;
        //        foreach (var j in joys)
        //        {
        //            if (j.supportsVibration)
        //                j.SetVibration(0.05f, 0.05f, Time.deltaTime * 5, Time.deltaTime * 5);
        //        }
        //    }
        //}

        internal void ResetPlayer(Transform resetLocation)
        {
            animator.SetTrigger(animatorResetParameter);
            transform.position = resetLocation.position;
            transform.rotation = resetLocation.rotation;
            targetRotation = resetLocation.eulerAngles;
            currentPoints = 0;
        }

        internal void SetDetails(PlayerDetails newDetails)
        {
            details = newDetails;

            playerRenderer.sharedMaterial = newDetails.playerMaterial;
            ring.material.SetColor("_Color", newDetails.playerColor);
            ring.material.SetColor("_EmissionColor", newDetails.playerColor * 2.5f);
            //foreach (var rend in renderers)
            //{
            //    var mats = rend.materials;
            //    foreach (var mat in mats)
            //    {
            //        mat.color = details.playerColor;
            //    }
            //    rend.materials = mats;
            //}
        }

        public void StartVibrationForNight()
        {
            foreach (Joystick joystick in input.controllers.Joysticks)
            {
                if(joystick.supportsVibration)
                    joystick.SetVibration(nightModeVibration, nightModeVibration);
            }
        }

        public void SetNightMat()
        {
            Timing.RunCoroutine(SetNightMatInternal());
        }

        IEnumerator<float> SetNightMatInternal()
        {
            float timer = 0f;
            float duration = 0.5f;

            while(timer < duration)
            {
                timer += Time.deltaTime;
                playerRenderer.sharedMaterial.SetFloat("_Emission", Mathf.Lerp(0.5f, 5f, timer / duration));
                yield return Timing.WaitForOneFrame;
            }
        }

        public void SetDayMat()
        {
            Timing.RunCoroutine(SetDayMatInternal());
        }

        IEnumerator<float> SetDayMatInternal()
        {
            float timer = 0f;
            float duration = 0.5f;

            while (timer < duration)
            {
                timer += Time.deltaTime;
                playerRenderer.sharedMaterial.SetFloat("_Emission", Mathf.Lerp(5f, 0.5f, timer / duration));
                yield return Timing.WaitForOneFrame;
            }
        }

        public void StopNightVibration()
        {
            foreach (Joystick joystick in input.controllers.Joysticks)
            {
                if(joystick.supportsVibration)
                    joystick.StopVibration();
            }
        }

        public void VibrateForPointLoss()
        {
            foreach (Joystick joystick in input.controllers.Joysticks)
            {
                if(joystick.supportsVibration)
                    joystick.SetVibration(pointLossVibration, pointLossVibration, pointLossVibrationDuration, pointLossVibrationDuration);
            }
        }

        public void SoftVibrate()
        {
            foreach (Joystick joystick in input.controllers.Joysticks)
            {
                if (joystick.supportsVibration)
                    joystick.SetVibration(pointLossVibration/2, pointLossVibration/2, pointLossVibrationDuration/2, pointLossVibrationDuration/2);
            }
        }

        public void TeleportToSpawn()
        {
            transform.position = spawnPosition;
            transform.rotation = Quaternion.identity;
            targetRotation = new Vector3(0.01f, 0.01f, 0.01f);
        }

        public void AutoTraverseToSpawn()
        {
            navAgent.enabled = true;
            navAgent.SetDestination(spawnPosition);
            navAgent.isStopped = false;
            Timing.RunCoroutine(WaitToDisableAgent());
        }

        IEnumerator<float> WaitToDisableAgent()
        {
            while (navAgent.remainingDistance >= navAgent.stoppingDistance)
                yield return Timing.WaitForOneFrame;
            navAgent.enabled = false;
        }

        internal void KnockBack(Vector3 hitDirection)
        {

        }
    }

}