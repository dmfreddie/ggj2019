﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

namespace GGJ
{

	public class DummyPlayer : MonoBehaviour
	{
		private int id;
		
		[SerializeField] private Renderer playerRenderer;
		[SerializeField] private Animator animator;
		public PlayerDetails currentDetails;
		private Player input;
		private bool ready = false;
		public bool Ready => ready;

		private int readyAnimation = Animator.StringToHash("Ready");

		private CharacterSelection cSelection;
		
		public void Init(CharacterSelection characterSelection, int playerID)
		{
			cSelection = characterSelection;
			id = playerID;
			input = ReInput.players.GetPlayer(playerID);
		}
		
		public void SetDetails(PlayerDetails newDetails, MeshRenderer ring)
		{
			currentDetails = newDetails;
			playerRenderer.sharedMaterial = newDetails.playerMaterial;
			ring.material.SetColor("_Color", newDetails.playerColor);
			ring.material.SetColor("_EmissionColor", newDetails.playerColor * 2.5f);
		}

		void Update()
		{
			
			if (input == null)
			{
				input = ReInput.players.GetPlayer(id);
				return;
			}
			
			if (input.GetButtonDown("ChangeMaterial"))
			{
				cSelection.ChangeColorScheme(id);
			}

			if (input.GetButtonDown("PlayGame"))
			{
				ready = !ready;
				if(animator)
					animator.SetBool(readyAnimation, ready);
				if(ready)
					cSelection.CheckReady();
			}

			if (input.GetButtonDown("Back"))
			{
				if (ready)
					ready = false;
				else
				{
					cSelection.RemoveCharacter(id);
				}
			}
		}

		public void SetPlayable()
		{
			input.controllers.maps.SetMapsEnabled(true, "Default");
			input.controllers.maps.SetMapsEnabled(false, "Assignment");

			if (input.controllers.hasKeyboard)
			{
				input.controllers.maps.LoadMap(ControllerType.Keyboard, 0, "Default", "Default", true);
				input.controllers.maps.LoadMap(ControllerType.Mouse, 0, "Default", "Default", true);
			}
		}
	}

}