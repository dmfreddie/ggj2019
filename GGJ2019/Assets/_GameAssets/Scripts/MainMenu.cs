﻿using MEC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GGJ
{

    public class MainMenu : MonoBehaviour
    {
        static MainMenu instance;
        public static MainMenu Instance {
            get
            {
                if (!instance)
                    instance = FindObjectOfType<MainMenu>();
                return instance;
            }
        }

        [Header("Fade Properties")]
        [SerializeField] private AnimationCurve fadeOutCurve = AnimationCurve.EaseInOut(0f, 0f, 0.5f, 1f);


        [Header("References")]
        [SerializeField] CanvasGroup mainMenuCanvasGroup;

        bool startingGame = false;

        private void Awake()
        {
            instance = this;
        }

//        private void OnEnable()
//        {
//            Timing.RunCoroutine(FadeIn());
//        }

        public void StartGame()
        {
            if (!startingGame)
            {
                startingGame = true;

                LoadingManager.Instance.LoadScene("CharacterSelection");
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        IEnumerator<float> FadeIn()
        {
            gameObject.SetActive(true);
            float timer = 0f;
            float duration = fadeOutCurve.keys[fadeOutCurve.length - 1].time;

            while (timer < duration)
            {
                timer += Time.deltaTime;
                mainMenuCanvasGroup.alpha = Mathf.Lerp(0f, 1f, fadeOutCurve.Evaluate(timer));
                yield return Timing.WaitForOneFrame;
            }
        }

//        IEnumerator<float> StartGameInternal(string targetScene)
//        {
//            startingGame = true;
//
//            LoadingManager.Instance.LoadScene(targetScene);
//
////            yield return Timing.WaitForOneFrame;
////            
////            float timer = 0f;
////            float duration = fadeOutCurve.keys[fadeOutCurve.length - 1].time;
////
////            while(timer < duration)
////            {
////                timer += Time.deltaTime;
////                mainMenuCanvasGroup.alpha = Mathf.Lerp(1f, 0f, fadeOutCurve.Evaluate(timer));
////                yield return Timing.WaitForOneFrame;
////            }
////
////            GameManager.Instance.StartGameFromMenuPressed();
////
////            gameObject.SetActive(false);
//        }
    }

}