﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    public class PlayerInteractionTrigger : MonoBehaviour
    {
        public float maxEnableTime = 0.25f;

        float timer = 0f;


        private void OnEnable()
        {
            timer = 0f;    
        }

        private void LateUpdate()
        {
            timer += Time.deltaTime;
            if(timer >= maxEnableTime)
            {
                gameObject.SetActive(false);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Player") )
            {
                other.GetComponent<PlayerController>().KnockBack((transform.position - other.transform.position).normalized);
                gameObject.SetActive(false);
            }
            else
            {
                // TODO
                gameObject.SetActive(false);
            }
        }

    }

}