﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{
    [RequireComponent(typeof(CanvasGroup))]
    public class FadeToBlack : MonoBehaviour
    {
        [SerializeField] private float fadeDuration;

        [SerializeField] CanvasGroup canvasGroup;
        MEC.CoroutineHandle fadeHandle;

        private static FadeToBlack instance;
        public static FadeToBlack Instance => instance;

        private void Awake()
        {
            if (!instance)
            {
                instance = this;
                canvasGroup = GetComponent<CanvasGroup>();
                DontDestroyOnLoad(this);
            }
            else
                Destroy(gameObject);
        }

        private void OnEnable()
        {
            FadeOut();
        }

        public void FadeIn()
        {
            if (fadeHandle.IsRunning)
                MEC.Timing.KillCoroutines(fadeHandle);

            fadeHandle = MEC.Timing.RunCoroutine(FadeIntrnal(true));
        }

        public void FadeOut()
        {
            if (fadeHandle.IsRunning)
                MEC.Timing.KillCoroutines(fadeHandle);

            fadeHandle = MEC.Timing.RunCoroutine(FadeIntrnal(false));
        }

        IEnumerator<float> FadeIntrnal(bool fadeIn)
        {
            float timer = 0f;
            var startFadeVal = canvasGroup.alpha;
            while(timer < fadeDuration)
            {
                timer += Time.deltaTime;
                canvasGroup.alpha = Mathf.Lerp(
                    startFadeVal,
                    fadeIn ? 1 : 0,
                    timer / fadeDuration);
                yield return MEC.Timing.WaitForOneFrame;
            }
        }
    }

}