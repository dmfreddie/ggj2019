﻿using Rewired;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GGJ
{
    public class PressStartToJoinPlayerSelector : MonoBehaviour
    {
        private int rewiredPlayerIdCounter = 0;

        // Track which Joysticks we've seen before in this session so we can tell new joysticks vs ones that have already been assigned to a Player
        private List<int> assignedJoysticks;

        //GameManager gameManager;
        bool haveMinPlayers = false;

        List<Player> connectedPlayers = new List<Player>();
        public List<Player> ConnectedPlayers => connectedPlayers;
        Player systemPlayer;

        public int MinPlayers = 1;
        public int MaxPlayers = 3;

        private CharacterSelection cSelection;
        
        void Awake()
        {
            assignedJoysticks = new List<int>();
            //gameManager = GetComponent<GameManager>();

            // Subscribe to controller connected events
            ReInput.ControllerConnectedEvent += OnControllerConnected;
        }

        void Start()
        {
            // NOTE: On some platforms/input sources, joysticks are ready at this time, but on others they may not be ready yet.
            // Must also check in OnControllerConected event.
            systemPlayer = ReInput.players.GetSystemPlayer();
            // Assign all Joysticks to the System Player initially removing assignment from other Players.
            AssignAllJoysticksToSystemPlayer(true);

            systemPlayer.controllers.maps.SetMapsEnabled(false, "Default");
            systemPlayer.controllers.maps.SetMapsEnabled(true, ControllerType.Keyboard, "Assignment");
            systemPlayer.controllers.maps.SetMapsEnabled(true, "Assignment");
            
            
            cSelection = FindObjectOfType<CharacterSelection>();
        }

        void OnControllerConnected(ControllerStatusChangedEventArgs args)
        {
            if (args.controllerType != ControllerType.Joystick) return;

            // Check if this Joystick has already been assigned. If so, just let Auto-Assign do its job.
            if (assignedJoysticks.Contains(args.controllerId)) return;

            // Joystick hasn't ever been assigned before. Make sure it's assigned to the System Player until it's been explicitly assigned
            systemPlayer.controllers.AddController<Joystick>(
                ReInput.controllers.GetJoystick(args.controllerId).id,
                true // remove any auto-assignments that might have happened
            );
        }

        public void ResetInput()
        {
            AssignAllJoysticksToSystemPlayer(true);
        }

        void AssignAllJoysticksToSystemPlayer(bool removeFromOtherPlayers)
        {
            foreach (var j in ReInput.controllers.Joysticks)
            {
                systemPlayer.controllers.AddController(j, removeFromOtherPlayers);
            }
        }

        void Update()
        {
            
            // Watch for JoinGame action in System Player
            if (systemPlayer.GetButtonDown("Join"))
            {
                AssignNextPlayer();
            }

//            if(haveMinPlayers)
//            {
//                if(ConnectedPlayers.Any(x => x.GetButtonDown("PlayGame")))
//                {
//                    gameManager.PlayGame();
//                    enabled = false;
//                }
//            }
        }

        void AssignNextPlayer()
        {
            if (rewiredPlayerIdCounter >= MaxPlayers)
            {
                Debug.Log("Max player limit already reached!");
                return;
            }

            // Get the next Rewired Player Id
            int rewiredPlayerId = GetNextGamePlayerId();

            // Get the Rewired Player
            Player rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);

            // Determine which Controller was used to generate the JoinGame Action
            //Player systemPlayer = ReInput.players.GetSystemPlayer();
            var inputSources = systemPlayer.GetCurrentInputSources("Join");

            foreach (var source in inputSources)
            {

                if (source.controllerType == ControllerType.Keyboard || source.controllerType == ControllerType.Mouse)
                { // Assigning keyboard/mouse

                    // Assign KB/Mouse to the Player
                    AssignKeyboardAndMouseToPlayer(rewiredPlayer);

                    // Disable KB/Mouse Assignment category in System Player so it doesn't assign through the keyboard/mouse anymore
                    systemPlayer.controllers.maps.SetMapsEnabled(false, ControllerType.Keyboard, "Assignment");
                    systemPlayer.controllers.maps.SetMapsEnabled(false, ControllerType.Mouse, "Assignment");
                    break;

                }
                else if (source.controllerType == ControllerType.Joystick)
                { // assigning a joystick

                    // Assign the joystick to the Player. This will also un-assign it from System Player
                    AssignJoystickToPlayer(rewiredPlayer, source.controller as Joystick);
                    break;

                }
                else
                { // Custom Controller
                    throw new System.NotImplementedException();
                }
            }

            // Enable UI map so Player can start controlling the UI
            

            if (rewiredPlayerIdCounter >= MinPlayers)
            {
                haveMinPlayers = true;
            }
            connectedPlayers.Add(rewiredPlayer);
            
            cSelection.AddCharacter(connectedPlayers.Count - 1);
            //gameManager.PlayerSelectionUI.AddNewPlayer(connectedPlayers.Count);
        }

        private void AssignKeyboardAndMouseToPlayer(Player player)
        {
            // Assign mouse to Player
            player.controllers.hasMouse = true;
            player.controllers.hasKeyboard = true;
            // Load the keyboard and mouse maps into the Player
            player.controllers.maps.LoadMap(ControllerType.Keyboard, 0, "Assignment", "Assignment", true);
            player.controllers.maps.LoadMap(ControllerType.Mouse, 0, "Assignment", "Assignment", true);

            // Exclude this Player from Joystick auto-assignment because it is the KB/Mouse Player now
            player.controllers.excludeFromControllerAutoAssignment = true;

            Debug.Log("Assigned Keyboard/Mouse to Player " + player.name);
        }

        private void AssignJoystickToPlayer(Player player, Joystick joystick)
        {
            // Assign the joystick to the Player, removing it from System Player
            player.controllers.AddController(joystick, true);

            if (joystick.supportsVibration)
                joystick.SetVibration(0.5f, 0.5f, 0.5f, 0.5f);

            // Mark this joystick as assigned so we don't give it to the System Player again
            assignedJoysticks.Add(joystick.id);

            Debug.Log("Assigned " + joystick.name + " to Player " + player.name);
        }

        private int GetNextGamePlayerId()
        {
            return rewiredPlayerIdCounter++;
        }
    }
}
