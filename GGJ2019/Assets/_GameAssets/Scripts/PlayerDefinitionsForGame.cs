#if UNITY_EDITOR
#endif
using UnityEngine;

namespace GGJ
{
    [CreateAssetMenu(menuName = "GGJ/PlayerDefinitionsForGame", fileName = "PlayerDefinitionsForGame")]
    public class PlayerDefinitionsForGame : ScriptableObject
    {
        public PlayerDetails[] currentPlayers;

        public PlayerDetails[] testPlayers;
    }
}