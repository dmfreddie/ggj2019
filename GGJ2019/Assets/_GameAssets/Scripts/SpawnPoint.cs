﻿using UnityEngine;

namespace GGJ
{
    public class SpawnPoint : MonoBehaviour
    {
        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 1, 0.65f);
            Gizmos.DrawCube(transform.position + Vector3.up, new Vector3(1, 2, 1));
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position + Vector3.up, new Vector3(1, 2, 1));
            DrawArrow.ForGizmo(transform.position + Vector3.up, transform.forward);
        }
    }

}