﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GGJ
{

    public class HAckedUIHAndler : MonoBehaviour
    {
        EventSystem current;
        GameObject currentSelected;
        public AudioSource uiSelection;

        // Use this for initialization
        void Start()
        {
            current = EventSystem.current;
        }

        // Update is called once per frame
        void Update()
        {
            if(current.currentSelectedGameObject != currentSelected)
            {
                currentSelected = current.currentSelectedGameObject;
                uiSelection.Play();
            }            
        }
    }

}