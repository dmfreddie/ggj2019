﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace GGJ
{

	public class CameraController : MonoBehaviour
	{

		[SerializeField] private Transform targetGamePosition;


		[SerializeField] private float playerZoomDistance = 2.5f;


		private Vector3 targetPosition;
		[Range(0.01f, 1f)][SerializeField] private float smoothTime = 0.5f;


		public Transform currentTarget;


		private Vector3 cachedStart;
		
		void Awake()
		{
			cachedStart = transform.position;
			targetPosition = cachedStart;
		}

		void Update()
		{
			Vector3 velocity = Vector3.zero;
			transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, 0.05f);
		}

		public void GoToNewTarget(Transform target)
		{
			targetPosition = target.position + Vector3.up + transform.forward * -playerZoomDistance;
			currentTarget = target;
		}

		public void ResetToGameCameraTarget()
		{
			targetPosition = cachedStart;
			currentTarget = null;
		}

		
	}

}