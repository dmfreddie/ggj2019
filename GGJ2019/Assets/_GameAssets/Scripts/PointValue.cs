﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace GGJ
{

    public class PointValue : MonoBehaviour
    {

        [SerializeField] private int value;

#if UNITY_EDITOR
        public Color debugColor;
#endif

        [HideInInspector] public PointsGroup owningGroup;
        [SerializeField] private AnimationCurve scaleInCurve;
        [SerializeField] private Transform rendererToScale;
        [SerializeField] private GameObject explosionVFXPrefab;
        ParticleSystem explosionVFX;
        float duration;
        Vector3 endsSize;
        CoroutineHandle hanlde;
        public AudioSource audioSource;

        private void Awake()
        {
            endsSize = rendererToScale.localScale;
            
        }

        private void Start()
        {
            explosionVFX = Instantiate(explosionVFXPrefab, transform.position, Quaternion.identity).GetComponent<ParticleSystem>();
            duration = scaleInCurve.keys[scaleInCurve.length - 1].time;
            audioSource.transform.SetParent(null);
        }

        private void OnEnable()
        {
            hanlde = Timing.RunCoroutine(ScaleIn());
        }

        private void OnDisable()
        {
            if (hanlde.IsRunning)
                Timing.KillCoroutines(hanlde);
        }

        private void OnTriggerEnter(Collider other)
        {
            var pc = other.GetComponent<PlayerController>();
            if (pc)
            {
                explosionVFX.Play(true);
                pc.AddPoints(value);
                owningGroup.RespawnNewPoint(gameObject);
                audioSource.Play();
            }
        }
        
        IEnumerator<float> ScaleIn()
        {
            float timer = 0f;
            while(timer < duration)
            {
                timer += Time.deltaTime;
                rendererToScale.transform.localScale = Vector3.LerpUnclamped(Vector3.zero, endsSize, scaleInCurve.Evaluate(timer));
                yield return Timing.WaitForOneFrame;
            }
        }
        
    }

}