﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ
{

    [System.Serializable]
    public struct EndStageSpawns
    {
        public GameObject rootSpawnComponent;
        public Transform spawnPoint;
        public TMPro.TextMeshPro scoreText;
    }

    public class EndStage : MonoBehaviour
    {
        [SerializeField] EndStageSpawns[] endStageSpawns;

        public void PopulateAndShow(ref List<PlayerController> orderedPlayers, ref int[] scores)
        {
            for (int i = 0; i < endStageSpawns.Length; ++i)
            {
                endStageSpawns[i].rootSpawnComponent.SetActive(false);
                endStageSpawns[i].scoreText.text = "";
            }

            for (int i = 0; i < orderedPlayers.Count; ++i)
            {
                orderedPlayers[i].GetComponent<CharacterController>().enabled = false;
                orderedPlayers[i].enabled = false;
                orderedPlayers[i].transform.position = endStageSpawns[i].spawnPoint.position;
                orderedPlayers[i].transform.rotation = endStageSpawns[i].spawnPoint.rotation;
                orderedPlayers[i].ring.gameObject.SetActive(false);
                endStageSpawns[i].scoreText.text = scores[i].ToString();
                endStageSpawns[i].rootSpawnComponent.SetActive(true);
            }

            gameObject.SetActive(true);
        }
    }

}