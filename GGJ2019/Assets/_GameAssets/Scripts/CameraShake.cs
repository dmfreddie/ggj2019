﻿using UnityEngine;
using System.Collections;

namespace GGJ
{

    public class CameraShake : MonoBehaviour
    {

        // Transform of the camera to shake. Grabs the gameObject's transform
        // if null.
        public Transform camTransform;

        public float shakeDuration = 1.0f;

        // How long the object should shake for.
        private float shakeDurationInternal = 0f;

        // Amplitude of the shake. A larger value shakes the camera harder.
        public float shakeAmount = 0.5f;

        public float decreaseFactor = 1.0f;

        Vector3 originalPos;

        public bool debug = false;

        CameraController cc;

        void Awake()
        {
            if (camTransform == null)
            {
                camTransform = GetComponent(typeof(Transform)) as Transform;
            }

            cc = FindObjectOfType<CameraController>();
        }

        void OnEnable()
        {
            originalPos = camTransform.localPosition;
            if (debug)
                Shake();
        }

        void Update()
        {
            if (shakeDurationInternal > 0)
            {
                cc.enabled = false;
                camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

                shakeDurationInternal -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                cc.enabled = true;
                shakeDurationInternal = 0f;
                camTransform.localPosition = originalPos;
            }
        }

        public void Shake()
        {
            shakeDurationInternal = shakeDuration;
            enabled = true;
        }
    }
}