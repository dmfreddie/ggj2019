﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace GGJ
{

	[RequireComponent(typeof(TMPro.TMP_Text))]
	public class PulseText : MonoBehaviour
	{
		private Color normalColor;

		[SerializeField] private Color pulseColor;

		private Vector3 startSize;

		[SerializeField] private Vector3 largeSize;

		[SerializeField] private AnimationCurve pulseCurve = AnimationCurve.EaseInOut(0f, 0f, 0.1f, 1f);

		private float duration;
		float timer = 0f;

		private TMPro.TMP_Text text;

		void Awake()
		{
			text = GetComponent<TMPro.TMP_Text>();
		}
		
		// Use this for initialization
		void Start()
		{
			duration = pulseCurve.keys[pulseCurve.length - 1].time;
			startSize = text.transform.localScale;
			normalColor = text.color;
			timer = duration + 1f;

			Timing.RunCoroutine(PulseInternal());
		}

		public void Pulse()
		{
			timer = 0;
		}

		IEnumerator<float> PulseInternal()
		{
			while (gameObject.activeInHierarchy)
			{
				while (timer < duration)
				{
					timer += Time.deltaTime;
					text.color = Color.Lerp(normalColor, pulseColor, pulseCurve.Evaluate(timer));
					text.transform.localScale = Vector3.Lerp(startSize, largeSize, pulseCurve.Evaluate(timer));
					yield return Timing.WaitForOneFrame;
				}

				text.color = normalColor;
				text.transform.localScale = startSize;
				yield return Timing.WaitForOneFrame;
			}
		}
	}

}