%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Bear_UpperBody
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Root
    m_Weight: 1
  - m_Path: Armature/Root/Hip_L
    m_Weight: 1
  - m_Path: Armature/Root/Hip_L/Leg_L
    m_Weight: 0
  - m_Path: Armature/Root/Hip_L/Leg_L/Shin_L
    m_Weight: 0
  - m_Path: Armature/Root/Hip_L/Leg_L/Shin_L/Foot_L
    m_Weight: 0
  - m_Path: Armature/Root/Hip_L/Leg_L/Shin_L/Foot_L/Foot_L_end
    m_Weight: 0
  - m_Path: Armature/Root/Hip_R
    m_Weight: 1
  - m_Path: Armature/Root/Hip_R/Leg_R
    m_Weight: 0
  - m_Path: Armature/Root/Hip_R/Leg_R/Shin_R
    m_Weight: 0
  - m_Path: Armature/Root/Hip_R/Leg_R/Shin_R/Foot_R
    m_Weight: 0
  - m_Path: Armature/Root/Hip_R/Leg_R/Shin_R/Foot_R/Foot_R_end
    m_Weight: 0
  - m_Path: Armature/Root/Spine
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_L
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_L/Arm_L
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_L/Arm_L/Wrist_L
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_L/Arm_L/Wrist_L/Wrist_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_R
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_R/Arm_R
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_R/Arm_R/Wrist_R
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Clavicle_R/Arm_R/Wrist_R/Wrist_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Neck
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Root/Spine/Spine.001/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Bear
    m_Weight: 1
