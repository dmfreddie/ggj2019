﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ
{
    public class ChangeColor : Button
    {
        public TMPro.TMP_Text text;

        public Color hoveredColor, normalColor;

        
        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            switch (state)
            {
                case Selectable.SelectionState.Normal:
                    text.color = normalColor;
                    break;
                case Selectable.SelectionState.Highlighted:
                    text.color = hoveredColor;
                    break;
            }
            base.DoStateTransition(state, instant);
        }
    }

}