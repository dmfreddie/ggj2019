// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/BearShader"
{
	Properties
	{
		_Bear_Base_Color("Bear_Base_Color", 2D) = "white" {}
		_Bear_Normal_OpenGL("Bear_Normal_OpenGL", 2D) = "white" {}
		_Bear_Mixed_AO("Bear_Mixed_AO", 2D) = "white" {}
		_Bear_Shirt_Mask("Bear_Shirt_Mask", 2D) = "white" {}
		[HDR]_StripeColour("StripeColour", Color) = (0.6985294,0.6985294,0.6985294,0)
		_ShirtColour("ShirtColour", Color) = (1,1,1,0)
		_Bear_Stripes_Mask("Bear_Stripes_Mask", 2D) = "white" {}
		_BearColour("BearColour", Color) = (1,1,1,0)
		_Emission("Emission", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Bear_Normal_OpenGL;
		uniform float4 _Bear_Normal_OpenGL_ST;
		uniform sampler2D _Bear_Base_Color;
		uniform float4 _Bear_Base_Color_ST;
		uniform float4 _BearColour;
		uniform float4 _ShirtColour;
		uniform float4 _StripeColour;
		uniform sampler2D _Bear_Stripes_Mask;
		uniform float4 _Bear_Stripes_Mask_ST;
		uniform sampler2D _Bear_Shirt_Mask;
		uniform float4 _Bear_Shirt_Mask_ST;
		uniform float _Emission;
		uniform sampler2D _Bear_Mixed_AO;
		uniform float4 _Bear_Mixed_AO_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Bear_Normal_OpenGL = i.uv_texcoord * _Bear_Normal_OpenGL_ST.xy + _Bear_Normal_OpenGL_ST.zw;
			o.Normal = tex2D( _Bear_Normal_OpenGL, uv_Bear_Normal_OpenGL ).rgb;
			float2 uv_Bear_Base_Color = i.uv_texcoord * _Bear_Base_Color_ST.xy + _Bear_Base_Color_ST.zw;
			float3 desaturateInitialColor17 = tex2D( _Bear_Base_Color, uv_Bear_Base_Color ).rgb;
			float desaturateDot17 = dot( desaturateInitialColor17, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar17 = lerp( desaturateInitialColor17, desaturateDot17.xxx, 1.0 );
			float4 temp_output_18_0 = ( float4( desaturateVar17 , 0.0 ) * _BearColour );
			float2 uv_Bear_Stripes_Mask = i.uv_texcoord * _Bear_Stripes_Mask_ST.xy + _Bear_Stripes_Mask_ST.zw;
			float4 lerpResult15 = lerp( _ShirtColour , _StripeColour , tex2D( _Bear_Stripes_Mask, uv_Bear_Stripes_Mask ));
			float2 uv_Bear_Shirt_Mask = i.uv_texcoord * _Bear_Shirt_Mask_ST.xy + _Bear_Shirt_Mask_ST.zw;
			float4 lerpResult9 = lerp( temp_output_18_0 , lerpResult15 , tex2D( _Bear_Shirt_Mask, uv_Bear_Shirt_Mask ));
			o.Albedo = lerpResult9.rgb;
			o.Emission = ( _Emission * temp_output_18_0 ).rgb;
			o.Metallic = 0.0;
			o.Smoothness = 0.2;
			float2 uv_Bear_Mixed_AO = i.uv_texcoord * _Bear_Mixed_AO_ST.xy + _Bear_Mixed_AO_ST.zw;
			o.Occlusion = tex2D( _Bear_Mixed_AO, uv_Bear_Mixed_AO ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
1928;27;1906;878;2149.231;1382.442;1.97612;True;False
Node;AmplifyShaderEditor.SamplerNode;1;-1607.341,-893.3057;Float;True;Property;_Bear_Base_Color;Bear_Base_Color;0;0;Create;True;0;0;False;0;1bda14ce42bc2164eb0b38342dd69b5d;1bda14ce42bc2164eb0b38342dd69b5d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DesaturateOpNode;17;-1265.973,-883.02;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;19;-1175.973,-604.02;Float;False;Property;_BearColour;BearColour;7;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;16;-1574.321,-221.5249;Float;True;Property;_Bear_Stripes_Mask;Bear_Stripes_Mask;6;0;Create;True;0;0;False;0;58cbe800f323ffc4ea751f704a9ac988;58cbe800f323ffc4ea751f704a9ac988;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;14;-1530.973,-424.02;Float;False;Property;_StripeColour;StripeColour;4;1;[HDR];Create;True;0;0;False;0;0.6985294,0.6985294,0.6985294,0;0.6985294,0.6985294,0.6985294,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;13;-1528.973,-592.02;Float;False;Property;_ShirtColour;ShirtColour;5;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;23;-879.3964,-878.9871;Float;False;Property;_Emission;Emission;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-882.9727,-694.02;Float;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;15;-1119.973,-408.02;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;8;-1073.286,-230.6991;Float;True;Property;_Bear_Shirt_Mask;Bear_Shirt_Mask;3;0;Create;True;0;0;False;0;6150debf9b52f64468413cf0f0b77c0a;6150debf9b52f64468413cf0f0b77c0a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-625.436,-780.3534;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-571.9727,112.98;Float;False;Constant;_Smoothness;Smoothness;3;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;9;-365.9727,-436.02;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-630,208;Float;True;Property;_Bear_Mixed_AO;Bear_Mixed_AO;2;0;Create;True;0;0;False;0;23f3c48af5d2f17449f9d6d2b0b93c2a;23f3c48af5d2f17449f9d6d2b0b93c2a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-634.5055,-165.3227;Float;True;Property;_Bear_Normal_OpenGL;Bear_Normal_OpenGL;1;0;Create;True;0;0;False;0;9d3913ebf28d21b40b2e2c79c6b967ca;9d3913ebf28d21b40b2e2c79c6b967ca;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;24;-382.7595,-244.7814;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-570.9727,35.97998;Float;False;Constant;_Metallic;Metallic;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;208.9485,-179.0987;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/BearShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;17;0;1;0
WireConnection;18;0;17;0
WireConnection;18;1;19;0
WireConnection;15;0;13;0
WireConnection;15;1;14;0
WireConnection;15;2;16;0
WireConnection;21;0;23;0
WireConnection;21;1;18;0
WireConnection;9;0;18;0
WireConnection;9;1;15;0
WireConnection;9;2;8;0
WireConnection;24;0;21;0
WireConnection;0;0;9;0
WireConnection;0;1;4;0
WireConnection;0;2;24;0
WireConnection;0;3;6;0
WireConnection;0;4;7;0
WireConnection;0;5;3;0
ASEEND*/
//CHKSM=1A9F9ABB03F4E940D1549EE78B3A467F7A2C1AC8