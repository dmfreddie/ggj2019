// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/WoodShader"
{
	Properties
	{
		_WoodenFloor_Diffuse("WoodenFloor_Diffuse", 2D) = "white" {}
		_WoodenFloor_Normal("WoodenFloor_Normal", 2D) = "white" {}
		_Desaturation("Desaturation", Float) = 0
		_Darkness("Darkness", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _WoodenFloor_Normal;
		uniform sampler2D _WoodenFloor_Diffuse;
		uniform float _Desaturation;
		uniform float _Darkness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TexCoord5 = i.uv_texcoord * float2( 5,5 );
			o.Normal = tex2D( _WoodenFloor_Normal, uv_TexCoord5 ).rgb;
			float3 desaturateInitialColor11 = tex2D( _WoodenFloor_Diffuse, uv_TexCoord5 ).rgb;
			float desaturateDot11 = dot( desaturateInitialColor11, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar11 = lerp( desaturateInitialColor11, desaturateDot11.xxx, _Desaturation );
			float4 color10 = IsGammaSpace() ? float4(0,0,0,0) : float4(0,0,0,0);
			float4 lerpResult6 = lerp( float4( desaturateVar11 , 0.0 ) , color10 , _Darkness);
			o.Albedo = lerpResult6.rgb;
			o.Metallic = 0.1;
			o.Smoothness = 0.6;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
5;22;1906;1124;2316.992;396.8854;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-1672.948,-143.029;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;5,5;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1333.347,-178.829;Float;True;Property;_WoodenFloor_Diffuse;WoodenFloor_Diffuse;0;0;Create;True;0;0;False;0;dcfec0e7b1e10b5429e81ba99b89f9fa;dcfec0e7b1e10b5429e81ba99b89f9fa;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-918.1765,-62.04004;Float;False;Property;_Desaturation;Desaturation;2;0;Create;True;0;0;False;0;0;0.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;11;-622.877,-153.0398;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-512.3766,-32.14001;Float;False;Property;_Darkness;Darkness;3;0;Create;True;0;0;False;0;0;0.51;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-626.7765,-342.84;Float;False;Constant;_Color0;Color 0;2;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;4;-330,192;Float;False;Constant;_Smoothness;Smoothness;2;0;Create;True;0;0;False;0;0.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;6;-241.9319,-167.3962;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-330,110;Float;False;Constant;_Metallic;Metallic;2;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1335.948,17.97103;Float;True;Property;_WoodenFloor_Normal;WoodenFloor_Normal;1;0;Create;True;0;0;False;0;37a2c86fb9f0cd242b79a64d66d68e19;37a2c86fb9f0cd242b79a64d66d68e19;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/WoodShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;1;1;5;0
WireConnection;11;0;1;0
WireConnection;11;1;12;0
WireConnection;6;0;11;0
WireConnection;6;1;10;0
WireConnection;6;2;8;0
WireConnection;2;1;5;0
WireConnection;0;0;6;0
WireConnection;0;1;2;0
WireConnection;0;3;3;0
WireConnection;0;4;4;0
ASEEND*/
//CHKSM=5141793715BC052F4F4D5D90BC9767ACED85D6FF