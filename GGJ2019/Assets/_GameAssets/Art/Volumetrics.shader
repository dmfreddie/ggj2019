// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "FB/Volumetrics"
{
	Properties
	{
		[HDR]_MainColor("MainColor", Color) = (1,1,1,0.828)
		_EmissiveStrength("EmissiveStrength", Float) = 1
		_FresnelScale("FresnelScale", Float) = 1
		_FresnelPower("FresnelPower", Float) = 5
		_DepthFade("DepthFade", Float) = 1
		_PanningSpeed("PanningSpeed", Vector) = (0.25,0.25,0,0)
		_PanningSpeed2("PanningSpeed2", Vector) = (-0.25,0.25,0,0)
		[HDR]_FogColor("FogColor", Color) = (0.6764706,0.6764706,0.6764706,0.522)
		_Noise("Noise", 2D) = "white" {}
		_Float0("Float 0", Float) = 0.25
		_VertexOffsetIntensity("VertexOffsetIntensity", Float) = 0
		_UVScale("UV Scale", Vector) = (1,1,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 4.6
		#pragma surface surf Standard alpha:fade keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			float4 screenPosition21;
		};

		uniform float _VertexOffsetIntensity;
		uniform sampler2D _Noise;
		uniform float2 _PanningSpeed;
		uniform float2 _UVScale;
		uniform float2 _PanningSpeed2;
		uniform float _Float0;
		uniform float4 _FogColor;
		uniform float4 _MainColor;
		uniform float _FresnelScale;
		uniform float _FresnelPower;
		uniform sampler2D _CameraDepthTexture;
		uniform float _DepthFade;
		uniform float _EmissiveStrength;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 ase_vertexNormal = v.normal.xyz;
			float2 uv_TexCoord27 = v.texcoord.xy * _UVScale;
			float2 panner26 = ( 1.0 * _Time.y * _PanningSpeed + uv_TexCoord27);
			float2 panner35 = ( 1.0 * _Time.y * _PanningSpeed2 + uv_TexCoord27);
			float4 blendOpSrc37 = tex2Dlod( _Noise, float4( panner26, 0, 0.0) );
			float4 blendOpDest37 = tex2Dlod( _Noise, float4( panner35, 0, 0.0) );
			float temp_output_41_0 = (( ( saturate( ( 1.0 - ( 1.0 - blendOpSrc37 ) * ( 1.0 - blendOpDest37 ) ) )) * _Float0 )).r;
			v.vertex.xyz += ( ( ase_vertex3Pos * ase_vertexNormal ) * ( ( ase_vertex3Pos.x * _SinTime.w ) * _VertexOffsetIntensity ) * temp_output_41_0 );
			float3 vertexPos21 = ase_vertex3Pos;
			float4 ase_screenPos21 = ComputeScreenPos( UnityObjectToClipPos( vertexPos21 ) );
			o.screenPosition21 = ase_screenPos21;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TexCoord27 = i.uv_texcoord * _UVScale;
			float2 panner26 = ( 1.0 * _Time.y * _PanningSpeed + uv_TexCoord27);
			float2 panner35 = ( 1.0 * _Time.y * _PanningSpeed2 + uv_TexCoord27);
			float4 blendOpSrc37 = tex2D( _Noise, panner26 );
			float4 blendOpDest37 = tex2D( _Noise, panner35 );
			float temp_output_41_0 = (( ( saturate( ( 1.0 - ( 1.0 - blendOpSrc37 ) * ( 1.0 - blendOpDest37 ) ) )) * _Float0 )).r;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV87 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode87 = ( 0.0 + _FresnelScale * pow( 1.0 - fresnelNdotV87, _FresnelPower ) );
			float clampResult93 = clamp( fresnelNode87 , 0.0 , 1.0 );
			float4 ase_screenPos21 = i.screenPosition21;
			float4 ase_screenPosNorm21 = ase_screenPos21 / ase_screenPos21.w;
			ase_screenPosNorm21.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm21.z : ase_screenPosNorm21.z * 0.5 + 0.5;
			float screenDepth21 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD( ase_screenPos21 ))));
			float distanceDepth21 = abs( ( screenDepth21 - LinearEyeDepth( ase_screenPosNorm21.z ) ) / ( _DepthFade ) );
			float temp_output_42_0 = ( temp_output_41_0 * ( 1.0 - clampResult93 ) * distanceDepth21 );
			float4 lerpResult94 = lerp( _FogColor , _MainColor , temp_output_42_0);
			o.Emission = ( lerpResult94 * _EmissiveStrength ).rgb;
			o.Alpha = temp_output_42_0;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
1931;101;1906;1004;3128.233;2311.861;2.250295;True;True
Node;AmplifyShaderEditor.Vector2Node;97;-3962.105,-2257.754;Float;False;Property;_UVScale;UV Scale;11;0;Create;True;0;0;False;0;1,1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;29;-3423.465,-2100.748;Float;False;Property;_PanningSpeed;PanningSpeed;5;0;Create;True;0;0;False;0;0.25,0.25;0.25,0.25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;39;-3519.888,-1834.6;Float;False;Property;_PanningSpeed2;PanningSpeed2;6;0;Create;True;0;0;False;0;-0.25,0.25;0.25,0.25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-3662.465,-2253.748;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;38;-3796.559,-2065.516;Float;True;Property;_Noise;Noise;8;0;Create;True;0;0;False;0;7e340e501ed86684c82966019e16d07b;7e340e501ed86684c82966019e16d07b;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.PannerNode;26;-3219.465,-2189.748;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;35;-3237.639,-1861.476;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-3175.904,-1289.953;Float;False;Property;_FresnelScale;FresnelScale;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-3192.904,-1213.953;Float;False;Property;_FresnelPower;FresnelPower;3;0;Create;True;0;0;False;0;5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;36;-3021.479,-1933.556;Float;True;Property;_TextureSample1;Texture Sample 1;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;25;-3003.305,-2261.828;Float;True;Property;_TextureSample0;Texture Sample 0;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;87;-2916.949,-1301.279;Float;False;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;37;-2640.745,-2026.633;Float;False;Screen;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;45;-2606.783,-1923.798;Float;False;Property;_Float0;Float 0;9;0;Create;True;0;0;False;0;0.25;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;22;-2012.62,-1227.93;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;23;-1960.62,-1045.93;Float;False;Property;_DepthFade;DepthFade;4;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;93;-2435.949,-1243.279;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-2365.003,-2026.112;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PosVertexDataNode;46;-2253.688,-838.377;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;50;-2199.031,-541.317;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;20;-2184.486,-1190.466;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;41;-2145.854,-1810.38;Float;True;True;False;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;21;-1779.62,-1148.93;Float;False;True;False;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1370.261,-1320.844;Float;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;-1679.415,-1998.876;Float;False;Property;_MainColor;MainColor;0;1;[HDR];Create;True;0;0;False;0;1,1,1,0.828;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;52;-1912.681,-454.7197;Float;False;Property;_VertexOffsetIntensity;VertexOffsetIntensity;10;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-1964.961,-640.075;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;47;-2241.806,-688.6663;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;31;-1724.442,-2178.809;Float;False;Property;_FogColor;FogColor;7;1;[HDR];Create;True;0;0;False;0;0.6764706,0.6764706,0.6764706,0.522;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;94;-1205.586,-1780.225;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1448.605,-1538.729;Float;False;Property;_EmissiveStrength;EmissiveStrength;1;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-1964.96,-783.7207;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;53;-1722.573,-577.1017;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;14;-3193.904,-1586.953;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;15;-3178.904,-1441.953;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;-1052.649,-1566.361;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1254.429,-714.9308;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-551.8578,-1561.539;Float;False;True;6;Float;ASEMaterialInspector;0;0;Standard;FB/Volumetrics;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Back;2;False;-1;1;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;True;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;15;10;25;True;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;0;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;27;0;97;0
WireConnection;26;0;27;0
WireConnection;26;2;29;0
WireConnection;35;0;27;0
WireConnection;35;2;39;0
WireConnection;36;0;38;0
WireConnection;36;1;35;0
WireConnection;25;0;38;0
WireConnection;25;1;26;0
WireConnection;87;2;16;0
WireConnection;87;3;17;0
WireConnection;37;0;25;0
WireConnection;37;1;36;0
WireConnection;93;0;87;0
WireConnection;44;0;37;0
WireConnection;44;1;45;0
WireConnection;20;0;93;0
WireConnection;41;0;44;0
WireConnection;21;1;22;0
WireConnection;21;0;23;0
WireConnection;42;0;41;0
WireConnection;42;1;20;0
WireConnection;42;2;21;0
WireConnection;51;0;46;1
WireConnection;51;1;50;4
WireConnection;94;0;31;0
WireConnection;94;1;1;0
WireConnection;94;2;42;0
WireConnection;48;0;46;0
WireConnection;48;1;47;0
WireConnection;53;0;51;0
WireConnection;53;1;52;0
WireConnection;96;0;94;0
WireConnection;96;1;5;0
WireConnection;54;0;48;0
WireConnection;54;1;53;0
WireConnection;54;2;41;0
WireConnection;0;2;96;0
WireConnection;0;9;42;0
WireConnection;0;11;54;0
ASEEND*/
//CHKSM=DCBCB9CA0AD2B7F25FE1B07E552CB97B185EAD56